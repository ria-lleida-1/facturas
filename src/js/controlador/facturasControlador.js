import $ from "jquery";

import { FacturasModelo } from "../modelo/facturasModelo";

export class FacturasControlador extends FacturasModelo {
    constructor() {
        super();
    }

    renderiza() {
        this.renderizaLis();
        this.renderizaTotales();    
    }

    renderizaLisPorTipo(tipo, selectorListado) {
        let $listaFacturas = $(selectorListado);
        this.getFacturasPorTipo(tipo)
                    .forEach(factura => {
                        let $liFactura = $("<li>");
                        $liFactura.text(this.getFacturaFormateada(factura));
                        $listaFacturas.append($liFactura);
                    });
        
    }

    renderizaLis() {
        this.removeLis();
        this.renderizaLisPorTipo("ingreso", ".listado.ingresos ul");
        this.renderizaLisPorTipo("gasto", ".listado.gastos ul");
    }

    removeLis() {
        $(".listado li").remove();
    }

    renderizaTotales() {
        let totalIVAIngresos = this.getTotalIvaPorTipo("ingreso");        
        let totalIVAGastos = this.getTotalIvaPorTipo("gasto");
        let totalBaseIngresos = this.getTotalBasePorTipo("ingreso");
        let totalBaseGastos = this.getTotalBasePorTipo("gasto");
        let totalTotalesIngresos = this.getTotalTotalesPortipo("ingreso");
        let totalTotalesGastos = this.getTotalTotalesPortipo("gasto");

        this.totalTotalesIngresos = totalTotalesIngresos;
        this.totalTotalesGastos = totalTotalesGastos;

        let balance = this.getBalance();

        $(".totales-ingresos .total-iva").text(totalIVAIngresos.toFixed(2));
        $(".totales-gastos .total-iva").text(totalIVAGastos.toFixed(2));
        $(".totales-ingresos .total-base").text(totalBaseIngresos.toFixed(2));
        $(".totales-gastos .total-base").text(totalBaseGastos.toFixed(2));
        $(".totales-ingresos .total-totales").text(totalTotalesIngresos.toFixed(2));
        $(".totales-gastos .total-totales").text(totalTotalesGastos.toFixed(2));
        $(".balance .balance-cantidad").text(balance.toFixed(2));
    
        if (balance >= 0) {
            $(".contenedor-balance").addClass("positivo").removeClass("negativo");
        } else {
            $(".contenedor-balance").addClass("negativo").removeClass("positivo");
        }
        
    }
 
    facturasCargadas() {
        this.renderiza();
    }

    initFormulario() {
        $(".form-nueva-factura").on("submit", (event) => {
            event.preventDefault();
            let numero = $(this).find("#numero").val();
            let base = Number($(this).find("#base").val());
            let iva = Number($(this).find("#iva").val());
            let tipo = $(this).find("#tipo").val();
        
            if (numero == "") {
                $("#numero").next(".mensaje").show();
            } else {
                $("#numero").next(".mensaje").hide();
            }
        
            let nuevaFactura = { numero: numero, baseImponible: base, iva: iva, tipo: tipo };
            this.anyadeFactura(nuevaFactura);            
            this.renderiza();
            let $formulario = $(".formulario");
            $formulario.removeClass("visible");
        })
        
        
        let $botonFormulario = $(".header .icono i");
        
        $botonFormulario.on("click", function() {                
            let $formulario = $(".formulario");
            $formulario.toggleClass("visible");
            $(this).toggleClass("fa-minus").toggleClass("fa-plus");                
        })                
    }
}