import "../scss/estilos.scss";
import "../../public/index.html";

import $ from "jquery";

import { Factura } from "./modelo/factura";
import { FacturasControlador } from "./controlador/facturasControlador";

let controladorFacturas = new FacturasControlador();

controladorFacturas.getFacturasAPI();
controladorFacturas.initFormulario();