export class Factura {

    constructor(numero, baseImponible, iva, tipo) {
        this.numero = numero;
        this.baseImponible = baseImponible;
        this.iva = iva;
        this.tipo = tipo;
    }

    get total() {
        return this.baseImponible * (1 + (this.iva / 100));
    }

    get totalIVA() {
        return this.baseImponible * this.iva / 100;
    }

}
