import $ from "jquery";

import { Factura } from "./factura";
import { config } from "../config/config";

export class FacturasModelo {
    constructor() {
        this.facturas;
        this.totalTotalesGastos;
        this.totalTotalesIngresos;
    }

    getFacturasAPI() {
        $.getJSON(config.apiUrl, datos => {            
            this.facturas = [];
            datos.forEach(dato => {
                let nuevaFactura = new Factura(dato.numero, dato.base, dato.iva, dato.tipo);
                this.facturas.push(nuevaFactura);                
            });            
            this.facturasCargadas();
        })        
    }

    postFacturaAPI(factura) {
        let nuevaFactura = {
            id: 4,
            numero: factura.numero,
            base: factura.base,
            iva: factura.iva,
            tipo: factura.tipo
        }
        $.post(config.apiUrl, nuevaFactura);
    }

    getFacturaFormateada(factura) {
        return `Num. ${factura.numero}: ${factura.total.toFixed(2)}€ 
                (Base ${factura.baseImponible}€ + IVA ${factura.iva}€)`;
    }

    getFacturasPorTipo(tipo) {
        return this.facturas.filter(factura => factura.tipo == tipo);
    }

    getTotalIvaPorTipo(tipo) {
        return this.getFacturasPorTipo(tipo)
            .map(factura => factura.totalIVA)
            .reduce( (totalIVA1, totalIVA2) => totalIVA1 + totalIVA2 );
    }

    getTotalBasePorTipo(tipo) {
        return this.getFacturasPorTipo(tipo)
            .map( factura => factura.baseImponible )
            .reduce( (base1, base2) => base1 + base2);
    }

    getTotalTotalesPortipo(tipo) {
        return this.getFacturasPorTipo(tipo)
            .map( factura => factura.total )
            .reduce( (total1, total2) => total1 + total2);
    }

    getBalance() {
        let balance = this.totalTotalesIngresos - this.totalTotalesGastos;
        return balance;
    }

    anyadeFactura(factura) {
        this.facturas.push(new Factura(factura.numero, factura.baseImponible, factura.iva, factura.tipo));
        this.postFacturaAPI(factura);
    }

    facturasCargadas() { }
}